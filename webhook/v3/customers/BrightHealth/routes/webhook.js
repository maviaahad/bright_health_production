const config = require('../../../common/libs/config-util');
const express = require('express');
const router = express.Router();

const WebhookController = require('../controllers/webhook');
const webhookController = new WebhookController();


const AccessToken = require('../middleware/accesstoken');
const accessToken = new AccessToken(config);

// router.get('/service',accessToken.getAccessToken, function(req,res){
//     res.setHeader('Content-Type', 'application/json');
//     res.send({"req.body.accessToken":req.body.accessToken});
//     res.end();
// })
// router.post('/service',accessToken.getAccessToken, webhookController.execute)

router.post('/service',webhookController.execute2)
router.post('/default', webhookController.callDefault)
router.post('/agent', webhookController.callAgent)
router.post('/confirm', webhookController.confirm)
router.post('/reset', webhookController.resetParam)
router.post('/firstname', webhookController.verifyFName)
router.post('/lastname', webhookController.verifyLName)


module.exports = router;