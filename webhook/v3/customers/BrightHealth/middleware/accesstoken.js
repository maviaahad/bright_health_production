const RestUtil = require('../../../common/libs/rest-util');


class AccessToken{
    constructor(config){
        this.config = config;
        this.restUtil = new RestUtil();
        this.getAccessToken=this.getAccessToken.bind(this);
    }

    async getAccessToken(req,res,next) {
        if(!req.body.accessToken){
            let options={
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "host":"sit-ext.api.anthem.com",
                    "Authorization":"Basic OTViZjQ4ZDUyYjE5NGU2OGFkODNmMWQzNjcyNDI0ZDA6NjgyMGZhYTBjMjgxYjhmNGMyYjgxMjZhNzY4MGNhNzk0ZGNhNDU3NDAzMDhlODQ2OWY0N2QxZGY2YzQ2MGYyNw==",
                    "apikey":"dzCAhhNYEnd2bjvANuAILyxfzyRlAKK8",
                    "cache-control":"no-cache"
                    // ,
                    // "meta-senderapp":"exlivr",  
                    // "meta-transid":req.body.session_id,
                    // "meta-vendorid":"EXLIVR"
                }
            }

            let data={
                grant_type:"client_credentials",
                scope:"public"
            }
            const path  = this.config.get("anthem:base_url")+this.config.get("anthem:token:url");
            const result = await this.restUtil.postRequest(path,Object.entries(data)
            .map(([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`)
            .join('&'),options);
            req.body.access_token=result.access_token; 
            console.log("access token is ================",result);
        }
        next();

    }

}

module.exports = AccessToken;