
const RestUtil = require('../../../common/libs/rest-util');
const CommonUtil = require('../../../common/libs/common-util');
const MongoUtil = require('../../../common/libs/mongo-util');
const XmljsonUtil = require('../../../common/libs/xmlJsonUtil');


class UserService {
    constructor(config) {
        this.config = config;
        this.mongoUtil = new MongoUtil(config.get('mongodb:url'));
        this.restUtil = new RestUtil();
    }

    postReq(key,param,data) {
        let req = this.config.get("anthem:"+key);
        let options = {
            headers: {
                "Content-Type": "application/xml",
                "Host": "sit.api.anthem.com",
                "Authorization": "Bearer " + param.accessToken,
                "apikey": req.apikey,
                "cache-control": "no-cache"
                // ,
                // "meta-senderapp":"exlivr",  
                // "meta-transid":param.session_id,
                // "meta-vendorid":"EXLIVR"

            }
        }

        const path = this.config.get("anthem:base_url");
        return this.restUtil.postRequest(path+req.url,data, options);
        // Object.entries(data)
        // .map(([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`)
        // .join('&')
    }

    async hippaVerification(param) {
        let result = await this.postReq("hippa_verification",param,{
            "tenantId": "AUTH", 
            "subscriberId": param.memberId,//"UPW000111838",//, 
            "serviceDate": CommonUtil.formatDate(), //"06-04-2019",//
            "serviceDateTo":"",  
            //"dateOfBirth": param.memberDOB,// "05-20-1967",
            "lastName":"", 
            "firstName":"",
            "stateCode":"", 
            "requestType":"",
            "caseType":""
        }
        )
        return result;
    }


    // async multipleAuthorizations(param){
    //     let json = {}

    //     if(param.check_status == "member_details"){
    //         json = { 
    //             "subscriberId": param.memberId,//"850A62670"
    //             "serviceDateStart":param.startDate,//"09-02-2019"
    //             "serviceDateEnd":param.endDate,//"09-30-2019"
    //             "patientDob":param.memberDOB, //"01-01-1970"
    //             "providerTaxId":param.taxId,//"263410591"  
    //             "idType":"NPI",  
    //             "idValue":param.npiValue
    //         }
    //     }else{
    //         json = { 
    //             "serviceDateStart":param.startDate,//"08-16-2019"
    //             "serviceDateEnd":param.endDate,//"08-18-2019"
    //             "requestType":"",
    //             "providerType":"", 
    //             "providerTaxId":param.taxId,  //"202305158",
    //             "idType":"NPI",  
    //             "idValue":param.npiValue //"1922098342" 
    //         }
    //     }
    //     let result = await this.postReq("summary",param,json)
    //     return result;
    // }


    async getData(param) {
     console.log("*****************************************")
        const xmlRequest = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://webservice.careradius.landacorp.com" xmlns:mem="http://memberservice.webservice.careradius.landacorp.com/">
   <soapenv:Header>
      <web:stateId>
         <!--Optional:-->
         <recordLockHolderName>?</recordLockHolderName>
         <recordLocked>?</recordLocked>
         <retrieveNoLock>?</retrieveNoLock>
         <!--Optional:-->
         <userContextId>CIVR#f76574fa-3286-4a79-85bb-a61faff31d0e</userContextId>
         <!--Optional:-->
         <workflowId>?</workflowId>
      </web:stateId>
   </soapenv:Header>
   <soapenv:Body>
      <mem:findMemberSummary>
         <!--Optional:-->
         <pCriteria>
            <!--Optional:-->
            <extensionProperties>
               <!--Zero or more repetitions:-->
               <item>
                  <!--Optional:-->
           
                  <!--Optional:-->
                  <value>?</value>
               </item>
            </extensionProperties>
            <!--Optional:-->
         
          
            <memberId>${param.memberId}</memberId>
  
         </pCriteria>
      </mem:findMemberSummary>
   </soapenv:Body>
</soapenv:Envelope>`;
        const opts = {
            body: xmlRequest,
            headers: {
                'Content-Type': 'text/xml; charset=utf-8',
                SOAPAction: ''
            }
        }
        const url = 'http://webservice.careradius.landacorp.com'
        const xmlResponse =await this.restUtil.postRequest(url, opts)
//         let xmlResponse=`<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
//    <S:Header>
//       <ns35:stateId xmlns:appeal="http://appeal.model.common.landacorp.com" xmlns:provider="http://provider.model.common.landacorp.com" xmlns:task="http://task.model.common.landacorp.com" xmlns:model="http://model.common.landacorp.com" xmlns:programEnrollment="http://programEnrollment.model.common.landacorp.com" xmlns:drg="http://drg.model.common.landacorp.com" xmlns:procedure="http://procedure.model.common.landacorp.com" xmlns:storage="http://storage.common.landacorp.com" xmlns:addressBook="http://addressBook.model.common.landacorp.com" xmlns:resourceprofile="http://resourceprofile.model.common.landacorp.com" xmlns:casemanagement_program="http://program.casemanagement.model.common.landacorp.com" xmlns:authorization="http://authorization.model.common.landacorp.com" xmlns:member360View="http://member360View.model.common.landacorp.com" xmlns:nsbusiness="http://xml.business.lib.landacorp.com" xmlns:clientServiceDetail="http://clientServiceDetail.model.common.landacorp.com" xmlns:member="http://member.model.common.landacorp.com" xmlns:codedesc="http://codedesc.model.common.landacorp.com" xmlns:attachment="http://attachment.model.common.landacorp.com" xmlns:userdefinedwindow="http://userdefinedwindow.model.careradius.landacorp.com" xmlns:note="http://note.model.common.landacorp.com" xmlns:criteria="http://criteria.model.common.landacorp.com" xmlns:casemanagement_careplan="http://careplan.casemanagement.model.common.landacorp.com" xmlns:clientDetail="http://clientdetail.model.common.landacorp.com" xmlns:contact="http://contact.model.common.landacorp.com" xmlns:clinicaldata="http://clinicaldata.model.common.landacorp.com" xmlns:memberNote="http://memberNote.model.common.landacorp.com" xmlns:diagnosis="http://diagnosis.model.common.landacorp.com" xmlns:nsxmlbean="http://bean.xml.lib.landacorp.com" xmlns:association="http://association.model.common.landacorp.com" xmlns:casemanagement="http://casemanagement.model.common.landacorp.com" xmlns:nsxml="http://xml.lib.landacorp.com" xmlns:ns33="http://resourceprofile.model.careradius.landacorp.com" xmlns:ns34="http://memberservice.webservice.careradius.landacorp.com/" xmlns:ns35="http://webservice.careradius.landacorp.com">
//          <recordLockHolderName>?</recordLockHolderName>
//          <recordLocked>false</recordLocked>
//          <retrieveNoLock>false</retrieveNoLock>
//          <userContextId>CIVR#f76574fa-3286-4a79-85bb-a61faff31d0e</userContextId>
//          <workflowId>?</workflowId>
//       </ns35:stateId>
//    </S:Header>
//    <S:Body>
//       <ns34:findMemberSummaryResponse xmlns:appeal="http://appeal.model.common.landacorp.com" xmlns:provider="http://provider.model.common.landacorp.com" xmlns:task="http://task.model.common.landacorp.com" xmlns:model="http://model.common.landacorp.com" xmlns:programEnrollment="http://programEnrollment.model.common.landacorp.com" xmlns:drg="http://drg.model.common.landacorp.com" xmlns:procedure="http://procedure.model.common.landacorp.com" xmlns:storage="http://storage.common.landacorp.com" xmlns:addressBook="http://addressBook.model.common.landacorp.com" xmlns:resourceprofile="http://resourceprofile.model.common.landacorp.com" xmlns:casemanagement_program="http://program.casemanagement.model.common.landacorp.com" xmlns:authorization="http://authorization.model.common.landacorp.com" xmlns:member360View="http://member360View.model.common.landacorp.com" xmlns:nsbusiness="http://xml.business.lib.landacorp.com" xmlns:clientServiceDetail="http://clientServiceDetail.model.common.landacorp.com" xmlns:member="http://member.model.common.landacorp.com" xmlns:codedesc="http://codedesc.model.common.landacorp.com" xmlns:attachment="http://attachment.model.common.landacorp.com" xmlns:userdefinedwindow="http://userdefinedwindow.model.careradius.landacorp.com" xmlns:note="http://note.model.common.landacorp.com" xmlns:criteria="http://criteria.model.common.landacorp.com" xmlns:casemanagement_careplan="http://careplan.casemanagement.model.common.landacorp.com" xmlns:clientDetail="http://clientdetail.model.common.landacorp.com" xmlns:contact="http://contact.model.common.landacorp.com" xmlns:clinicaldata="http://clinicaldata.model.common.landacorp.com" xmlns:memberNote="http://memberNote.model.common.landacorp.com" xmlns:diagnosis="http://diagnosis.model.common.landacorp.com" xmlns:nsxmlbean="http://bean.xml.lib.landacorp.com" xmlns:association="http://association.model.common.landacorp.com" xmlns:casemanagement="http://casemanagement.model.common.landacorp.com" xmlns:nsxml="http://xml.lib.landacorp.com" xmlns:ns33="http://resourceprofile.model.careradius.landacorp.com" xmlns:ns34="http://memberservice.webservice.careradius.landacorp.com/" xmlns:ns35="http://webservice.careradius.landacorp.com">
//          <return>
//             <address>7702 Main Street</address>
//             <coverageList>
//                <createdTimeStamp>2019-08-12T16:13:09-04:00</createdTimeStamp>
//                <lastUpdateTimeStamp>2020-01-31T09:48:20-05:00</lastUpdateTimeStamp>
//                <recordVersion>3</recordVersion>
//                <userUidCreatedBy>4</userUidCreatedBy>
//                <userUidUpdatedBy>4</userUidUpdatedBy>
//                <client>
//                   <clientId>BHPIFPFL</clientId>
//                   <internalId>6</internalId>
//                   <name>BHP IFP Florida</name>
//                </client>
//                <clientDivision>
//                   <divisionId>BHPIFPFLJAXPEN</divisionId>
//                </clientDivision>
//                <cobra>false</cobra>
//                <cobraBeginDate>1800-01-01T00:00:00-05:00</cobraBeginDate>
//                <cobraEndDate>2099-12-31T00:00:00-05:00</cobraEndDate>
//                <coverageTerminated>false</coverageTerminated>
//                <creditDateBegin>1800-01-01T00:00:00-05:00</creditDateBegin>
//                <creditDateEnd>2099-12-31T00:00:00-05:00</creditDateEnd>
//                <dateBegin>2019-01-01T00:00:00-05:00</dateBegin>
//                <dateEnd>2099-12-31T00:00:00-05:00</dateEnd>
//                <directDeduction>false</directDeduction>
//                <internalId>1</internalId>
//                <lateEnrollment>false</lateEnrollment>
//                <member>
//                   <internalId>1</internalId>
//                </member>
//                <payerPlan>
//                   <description>73% AV Level Silver Plan</description>
//                   <payerPlanId>16985SC001000304</payerPlanId>
//                </payerPlan>
//                <preExistingCredit>false</preExistingCredit>
//                <providerNetwork>
//                   <code>PVN047</code>
//                </providerNetwork>
//                <reinsurance>false</reinsurance>
//                <student>false</student>
//                <subrogation>false</subrogation>
//             </coverageList>
//             <defaultBusinessEntityDescription>BHP IFP Florida - Jacksonville/Pensacola</defaultBusinessEntityDescription>
//             <defaultBusinessEntityId>BHPIFPFLJAXPEN</defaultBusinessEntityId>
//             <defaultClientAssociationInternalId>20</defaultClientAssociationInternalId>
//             <defaultClientId>BHPIFPFL</defaultClientId>
//             <defaultClientInternalId>6</defaultClientInternalId>
//             <defaultClientName>BHP IFP Florida</defaultClientName>
//             <defaultClientServiceGroupInternalId>6</defaultClientServiceGroupInternalId>
//             <defaultClientServiceGroupName>BHP IFP Florida</defaultClientServiceGroupName>
//             <defaultCoverageInternalId>1</defaultCoverageInternalId>
//             <defaultPayerPlanEffectiveDate>2019-01-01T00:00:00-05:00</defaultPayerPlanEffectiveDate>
//             <defaultPayerPlanId>16985SC001000304</defaultPayerPlanId>
//             <defaultPayerPlanName>73% AV Level Silver Plan</defaultPayerPlanName>
//             <defaultPayerPlanTerminationDate>2099-12-31T00:00:00-05:00</defaultPayerPlanTerminationDate>
//             <homePhone>909-935-5060</homePhone>
//             <internalId>1</internalId>
//             <memberDateOfBirth>1999-10-05</memberDateOfBirth>
//             <memberFirstName>WINNIE</memberFirstName>
//             <memberFormattedName>AIHUA, WINNIE</memberFormattedName>
//             <memberGender>FEMALE</memberGender>
//             <memberId>9099355060</memberId>
//             <memberLastName>AIHUA</memberLastName>
//             <postalCode>32256</postalCode>
//             <stateCode>FL</stateCode>
//             <validationClientInternalId>6</validationClientInternalId>
//          </return>
//       </ns34:findMemberSummaryResponse>
//    </S:Body>
// </S:Envelope>`

        let xmltojson=XmljsonUtil.xmltojson(xmlResponse,"memberId")
        console.log("--------------------------------------------000____________________________")
        console.log(xmltojson[0].memberId)
        console.log(param.memberId)
        return xmltojson
        // if(xmltojson[0].memberId[0]===param.memberId){return xmltojson}





        // let req = this.config.get("rgil:" + key);
        //     let options = {
        //         headers: {
        //             "Content-Type": "text/xml",
        //         }
        //     }
        //
        //     let path;
        //
        //
        //     if (param. == "policynumber") {
        //
        //         // const path = this.config.get(req+":url"+`${param.parameters.policynumber}`);
        //
        //         path = req.url + data;
        //         console.log("-----------path is ", path);
        //         return this.restUtil.getRequest(path, options);
        //
        //     } else if (type == "policyOnDigit") {
        //
        //         let path1 = "http://rgidwh01.reliancegeneral.co.in/rgiwebservice/Service1.asmx?wsdl";
        //         let d = await this.restUtil.postRequest(path1, data, options);
        //         let r = XmljsonUtil.xmltojson(d, type);
        //         console.log("----------------result-----------", r);
        //         return r;
        //
        //     }



    }



    async getCaseRef(param) {
        let result = await this.postReq("summary",param, {  
            "caseRequestId":param.refNumber,//"UM5486927",//
            "providerTaxId":param.taxId //"202305158",//
        })
        return result;
    }

}

module.exports = UserService;