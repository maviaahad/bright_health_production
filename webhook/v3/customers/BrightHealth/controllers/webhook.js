const config = require('../../../common/libs/config-util');
const NlpHandler = require('../../../common/nlp/nlp-handler');
const ConstantUtil = require('../../../common/libs/constant-util');
const MessageUtil = require('../../../common/libs/message-util');
const xmlUtil = require('../../../common/libs/xmlJsonUtil');
const UserService = require('../services/users');

let memberInfoFetched= '';
class WebhookController {
    constructor() {
        this.userService = new UserService(config);
        this.nlpHandler = new NlpHandler(config);
        this.callAgent = this.callAgent.bind(this);
        this.execute = this.execute.bind(this);
        this.execute2 = this.execute2.bind(this);
        this.callDefault = this.callDefault.bind(this);
        this.confirm = this.confirm.bind(this);
        this.resetParam = this.resetParam.bind(this);
        this.verifyFName = this.verifyFName.bind(this);
        this.verifyLName = this.verifyLName.bind(this);
    }
    async confirm(parameters,flowName,intentName,param) {
        try {
            let val = this.correctForPolly(parameters[ConstantUtil.getFlowMapping()[flowName].key])
            // if(flowName == 'member_id_flow' || flowName == 'service_start_date'){
            //     let dt=parameters[ConstantUtil.getFlowMapping()[flowName].key];
            //     val = this.dateFormat(dt);
            //     console.log("date confirm ========= ",val);
            // }
            let msg = `Just to confirm ${ConstantUtil.getFlowMapping()[flowName].field} is ${val} . Is that Correct ?`
            return {msg: msg,nextFlow:""}
        } catch (e) {
            throw Error
        }
    };

    async getDataOnMemberID(parameters,flowName,intentName,param){

        try{
        let result = await this.userService.getData(param);
        let data = {msg:"",nextFlow:"",resetEntities:[],agent:false};
        if (result && result[0]) {
            param.PARAMETERS.fName = result[0].memberFirstName;
            param.PARAMETERS.lName = result[0].memberLastName;
            param.PARAMETERS.dob = result[0].memberDateOfBirth;
            param.PARAMETERS.phone = result[0].homePhone;
            param.PARAMETERS.npiID = result[0].npi_id;
            param.PARAMETERS.auth_id = result[0].auth_id;
            param.PARAMETERS.Status = result[0].Status;
            param.PARAMETERS.startD = result[0].start_date;
            param.PARAMETERS.streetName = result[0].street_name;
            param.PARAMETERS.ZIP = result[0].zip;
            param.PARAMETERS.address = result[0].address;
            param.PARAMETERS.error_count = param.PARAMETERS.error_count?param.PARAMETERS.error_count : 0;
            if (param.memberId===result[0].memberId[0]) {

                data.msg = "Okay"
                data.nextFlow = ConstantUtil.getFlows().MEMBER_NAME
            } else {
                param.PARAMETERS.error_count = param.PARAMETERS.error_count ? param.PARAMETERS.error_count : 0
                param.PARAMETERS.error_count=param.PARAMETERS.error_count+1;
                if (param.PARAMETERS.error_count >= config.get("agent:error_count")) {
                    data.msg = MessageUtil.getMessage().AGENT_TRANSFER
                    data.nextFlow = ConstantUtil.getFlows().AGENT_TRANSFER
                    // data.agent = true
                    //Added by Ayush
                    // param.PARAMETERS.subscriber_id_count = 0;
                    // json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Member id "+val+". Let's try with NPI number. ");
                    // json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                    // json.parameters.agent = true;
                } else {
                    data.msg = MessageUtil.getMessage().MEMBER_NOT_FOUND
                    data.nextFlow = ConstantUtil.getFlows().MEMBER_ID_FLOW
                    data.resetEntities.push('subscriber_id')
                }

            }

            return data;
        }}catch(err){
            throw Error
        }
    }

    async verifyFName(parameters,flowName,intentName,param){
        try {
            let data = {msg:"",nextFlow:"",resetEntities:[],agent:false};
            let isMatched=parameters.first_name_characters.toLowerCase().replace(/ /g, "") === parameters.fName[0].toLowerCase().substr(0,3);
            if(isMatched){
                data.msg = "Okay"
                data.nextFlow = ConstantUtil.getFlows().MEMBER_LNAME
            }else{
                param.PARAMETERS["Fname_error_name_count"]=param.PARAMETERS["Fname_error_name_count"]?param.PARAMETERS["Fname_error_name_count"]:0;
                param.PARAMETERS["Fname_error_name_count"]++;
                if(param.PARAMETERS["Fname_error_name_count"] >= config.get("agent:error_count")){
                    param.PARAMETERS.error_count++;
                    data.msg = MessageUtil.getMessage().FIRST_NAME_FAIL
                    data.nextFlow = ConstantUtil.getFlows().AUTH_FLOW
                }else{
                    data.msg = MessageUtil.getMessage().FIRST_NAME_FAIL
                    data.nextFlow = ConstantUtil.getFlows().MEMBER_NAME
                    data.resetEntities.push('first_name_characters')
                }
            }
            return data
        }catch(err){
            throw Error
        }
    }

    async verifyLName(parameters,flowName,intentName,param){
        try {
            let data = {msg:"",nextFlow:"",resetEntities:[],agent:false};
            let isMatched=parameters.last_name_characters.toLowerCase().replace(/ /g, "") === parameters.lName[0].toLowerCase().substr(0,3);
            if(isMatched){
                data.msg = "Okay"
                data.nextFlow = ConstantUtil.getFlows().AUTH_FLOW
            }else{
                param.PARAMETERS["Lname_error_name_count"]=param.PARAMETERS["Lname_error_name_count"]?param.PARAMETERS["Lname_error_name_count"]:0;
                param.PARAMETERS["Lname_error_name_count"]++;
                if(param.PARAMETERS["Lname_error_name_count"] >= config.get("agent:error_count")){
                    param.PARAMETERS.error_count++;
                    data.msg = MessageUtil.getMessage().LAST_NAME_FAIL
                    data.nextFlow = ConstantUtil.getFlows().AUTH_FLOW
                }else{
                    data.msg = MessageUtil.getMessage().LAST_NAME_FAIL
                    data.nextFlow = ConstantUtil.getFlows().MEMBER_LNAME
                    data.resetEntities.push('last_name_characters')
                }
            }
            return data
        }catch(err){
            throw Error
        }
    }

    async verifyDateOfBirth(parameters,flowName,intentName,param){
        try {
            let data = {msg: "", nextFlow: "", resetEntities: [], agent: false};
            let dob = param.PARAMETERS.dob[0].split('-').reverse()
            let temp = dob[0]
            dob[0] = dob[1]
            dob[1] = temp
            let isMatched = dob.join('-') === param.PARAMETERS.date_of_birth
            if (isMatched) {
                console.log("-------------------------------auth-----------------------------------")
                console.log(param.PARAMETERS.error_count)
                console.log("-------------------------------auth-----------------------------------")
                data.msg = MessageUtil.getMessage().NAME_SUCCESS
                data.nextFlow = param.PARAMETERS.error_count > 0 ? ConstantUtil.getFlows().MEMBER_PHONE : ''
            } else {
                param.PARAMETERS.dob_error_count = param.PARAMETERS.dob_error_count ? param.PARAMETERS.dob_error_count : 0
                param.PARAMETERS.dob_error_count++;
                if (param.PARAMETERS.dob_error_count >= config.get("agent:error_count")) {
                    param.PARAMETERS.error_count++
                    data.msg = param.PARAMETERS.error_count === 2 ? MessageUtil.getMessage().AGENT_TRANSFER : MessageUtil.getMessage().MEMBER_DOB_NOT_FOUND
                    data.nextFlow = param.PARAMETERS.error_count === 2 ? ConstantUtil.getFlows().AGENT_TRANSFER : ConstantUtil.getFlows().MEMBER_PHONE
                    // data.agent = param.PARAMETERS.error_count === 2
                    //Added by Ayush
                    // param.PARAMETERS.subscriber_id_count = 0;
                    // json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Member id "+val+". Let's try with NPI number. ");
                    // json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                    // response.parameters.agent = true;
                } else {
                    data.msg = MessageUtil.getMessage().MEMBER_DOB_NOT_FOUND
                    data.nextFlow = ConstantUtil.getFlows().AUTH_FLOW
                    data.resetEntities.push('date_of_birth')
                }
            }
            return data
        }catch(err){
            throw Error
        }

    }

    async verifyPhoneNumber(parameters,flowName,intentName,param){
        try{
            let data = {msg: "", nextFlow: "", resetEntities: [], agent: false};
            let isMatched = param.PARAMETERS.phone[0].split('-').join("") === param.PARAMETERS.mobile_number
            if(isMatched){
                data.msg = MessageUtil.getMessage().PHONE_SUCCESS
                data.nextFlow=''
            }else{
                param.PARAMETERS.phone_error_count = param.PARAMETERS.phone_error_count ? param.PARAMETERS.phone_error_count : 0
                param.PARAMETERS.phone_error_count++;
                if (param.PARAMETERS.phone_error_count >= config.get("agent:error_count")) {
                    data.msg=MessageUtil.getMessage().AGENT_TRANSFER
                    data.nextFlow=ConstantUtil.getFlows().AGENT_TRANSFER
                    // data.agent=true
                }else{
                    data.msg=MessageUtil.getMessage().MEMBER_PHONE_NOT_FOUND
                    data.nextFlow=ConstantUtil.getFlows().MEMBER_PHONE
                    data.resetEntities=['mobile_number']
                }
            }
            return data}catch(err){
            throw Error
        }

    }

    async verifyName(parameters,flowName,intentName,param) {
        console.log("+++++++++++++++++++++++FNAME+++++++++++++++++++++++++++++++++++++++++++++++++++===")
        console.log(req.body)
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try {
            let response = {}, reqbody = req.body, isMatched=false;
            let parameters = reqbody.parameters;
            parameters = parameters ? parameters : {};
            let msg = '',flowName=null;
            if(type == 'first'){
                console.log("fName:",parameters.fName[0])
                console.log("first_name_characters:",parameters.first_name_characters)
                // parameters.fName=parameters.fName?parameters.fName[0]:"";
                // parameters.first_name_characters=parameters.first_name_characters?parameters.first_name_characters:"";
                isMatched=parameters.first_name_characters.toLowerCase().replace(/ /g, "") === parameters.fName[0].toLowerCase().substr(0,3);
                msg = isMatched?"Okay":MessageUtil.getMessage().FIRST_NAME_FAIL;
                flowName = ConstantUtil.getFlows()[isMatched?'MEMBER_LNAME':'MEMBER_PHONE']
                console.log(isMatched)
                console.log(parameters.first_name_characters.toLowerCase().replace(/ /g, ""))
            }else{
               
                // parameters.lName=parameters.lName?parameters.lName[0]:"";
                // parameters.last_name_characters=parameters.last_name_characters?parameters.last_name_characters:"";
                isMatched=parameters.last_name_characters.toLowerCase().replace(/ /g, "") === parameters.lName[0].toLowerCase().substr(0,3);
                msg = isMatched?"Okay":MessageUtil.getMessage().LAST_NAME_FAIL;
                flowName = ConstantUtil.getFlows()[isMatched?'AUTH_FLOW':'MEMBER_PHONE'];
                //flowName = ConstantUtil.getFlows()[isMatched?'MEMBER_ADDRESS':'MEMBER_LNAME'];
            }
            const param = this.nlpHandler.fetchNlpParams(reqbody);
            response = this.nlpHandler.fetchTextJson(msg);
            response = this.nlpHandler.fetchFinalResponse(response, param,flowName);
            if(isMatched){
                response.parameters[type+"_name_count"]=0;
                response.parameters.confirmed_entities++
            }else{
                response.parameters[type+"_name_count"]=response.parameters[type+"_name_count"]?response.parameters[type+"_name_count"]:0;
                response.parameters[type+"_name_count"]++;
                //MEMBER_PHONE
                if(response.parameters[type+"_name_count"] >= config.get("agent:error_count")){
                    // response.followup_event="";
                    // response.response.text= response.response.text + " " + MessageUtil.getMessage().NAME_FAIL;
                    // response.parameters.agent=true;
                    response.parameters.error_count++;
                    response = this.nlpHandler.fetchTextJson(MessageUtil.getMessage().NAME_FAIL);
                    response = this.nlpHandler.fetchFinalResponse(response, param,ConstantUtil.getFlows().AUTH_FLOW);
                }else{

                    response = this.nlpHandler.fetchTextJson(MessageUtil.getMessage().NAME_FAIL);
                    response = this.nlpHandler.fetchFinalResponse(response, param,type === 'first'?ConstantUtil.getFlows().MEMBER_NAME:ConstantUtil.getFlows().MEMBER_LNAME);
                    response.reset_entities= type === 'first'?['first_name_characters']:['last_name_characters']

                }
            }
            console.log(" response --------------- ", response)
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            res.end();
        } catch (e) {
            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    };

    async resetParam(request, res) {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try {
            let response = {}, reqbody = request.body, flowName = reqbody.flow_name;
            let cnfg = ConstantUtil.getResetMapping()[flowName];
            const param = this.nlpHandler.fetchNlpParams(reqbody);
            if(param.PARAMETERS[cnfg.key+"_count"] || param.PARAMETERS[cnfg.key+"_count"] == 0){
                param.PARAMETERS[cnfg.key+"_count"]++;
            }else{
                param.PARAMETERS[cnfg.key+"_count"]=0;
                param.PARAMETERS[cnfg.key+"_count"]++;
            }
            console.log(cnfg.key+"_count ====== ",param.PARAMETERS[cnfg.key+"_count"]);
            if(param.PARAMETERS[cnfg.key+"_count"] >= config.get("agent:error_count")){
                response = await this.nlpHandler.fetchTextJson(cnfg.message+ "" +MessageUtil.getMessage().AGENT_TRANSFER);
                response = await this.nlpHandler.fetchFinalResponse(response, param);
                response.parameters.agent = true;
            }else{
                response = this.nlpHandler.fetchTextJson('Okay');
                response = this.nlpHandler.fetchFinalResponse(response, param,cnfg.redirect);
                response.reset_entities=[cnfg.key];
            }
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            console.log(" response --------------- ", response)
            res.end();
        } catch (e) {
            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    };

    async callAgent(request, res) {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try {
            let response = {}, reqbody = request.body;
            const param = this.nlpHandler.fetchNlpParams(reqbody);
            param.PARAMETERS.agent = true;
            response = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
            response = await this.nlpHandler.fetchFinalResponse(response, param);
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            res.end();
        } catch (e) {
            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    };

    async execute(request, res) {
        console.log("++++++++++++++++++++++++++&&&&&&&&&&&&&&&&&7++++++++++++++++++++++++++++++++++++++++++++++++++++")
        console.log(request.body)
        console.log("++++++++++++++++++++++++++&&&&&&&&&&&&&&&&&7++++++++++++++++++++++++++++++++++++++++++++++++++++")
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try {
            let response = {}
            let reqbody = request.body;
            let parameters = reqbody.parameters;
            parameters = parameters ? parameters : {}
            const intentName = reqbody.intent_name
            const flowName = reqbody.flow_name;
            const param = this.nlpHandler.fetchNlpParams(reqbody);
            param.session_id=reqbody.session_id;
            param.accessToken = reqbody.access_token;//----------------
            param.flowName = flowName;
            param.intentName = intentName;
            param.startDate = parameters.start_date;//--------------
            param.start_date = parameters.startD;//---------------
            param.endDate = parameters.end_date;//-------------
            param.date_of_birth=parameters.date_of_birth;//----------------
            param.memberId = this.validateParam(parameters.subscriber_id);
            param.memberDOB = parameters.date_of_birth;//----------------
            param.refNumber = this.validateParam(parameters.c_ref);//----------------
            param.authID = parameters.auth_id;//----------------
            param.npiID = parameters.npiID;//----------------
            param.npiValue = this.validateParam(parameters.npi_id);//----------------
            param.house_number = parameters.house_number;//----------------
            param.houseNumber=parameters.houseNumber;//----------------
            param.street_name = parameters.street_name;//----------------
            param.streetName=parameters.streetName;//----------------
            param.taxId = this.validateParam(parameters.tax_id);//----------------
            param.ZIP=parameters.ZIP;//----------------

            param.check_status = parameters.check_status_type;//----------------
            delete param.PARAMETERS.agent;//----------------
            // console.log("----------------------------- inside", param.memberId)

            // console.log("request body ",reqbody)


         if (flowName == ConstantUtil.getFlows().MEMBER_ID_YES) {
                //response = await this.hippaVerification(param);
                response = await this.getDataOnMemberID(param)


            }


         else if (intentName == ConstantUtil.getIntents().NO_MAIN_MENU) {
                response = await this.nlpHandler.fetchTextJson("Okay");
                response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().MEMBER_ID_FLOW);
                response.reset_entities = ['c_ref_count','error_count','npi_id','npi_id_count','check_status_type','c_ref','tax_id','tax_id_count','subscriber_id','subscriber_id_count', 'start_date', 'start_date_count', 'end_date', 'end_date_count', 'date_of_birth', 'date_of_birth_count', 'first_name_characters', 'first_name_characters_count', 'last_name_characters', 'last_name_characters_count','ENT_UI_5005_count','ENT_UI_5006_count','ENT_UI_5007_count','ENT_ME_62_count','ENT_ME_71_count','ENT_ME_72_count','ENT_ME_1003_count','ENT_ME_router_2100_count'];
            }
            
            else if (flowName == ConstantUtil.getFlows().SAME_MEMBER) {
                response = await this.nlpHandler.fetchTextJson("Okay");
                response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().MAIN_MENU_FLOW);
                response.reset_entities = ['c_ref_count','error_count','npi_id','npi_id_count','check_status_type','c_ref','c_ref_count', 'start_date', 'start_date_count', 'end_date', 'end_date_count','ENT_UI_5005_count','ENT_UI_5006_count','ENT_UI_5007_count','ENT_ME_62_count','ENT_ME_71_count','ENT_ME_72_count','ENT_ME_1003_count','ENT_ME_router_2100_count'];
            }
            else if (flowName == ConstantUtil.getFlows().AGENT_TRANSFER || intentName == ConstantUtil.getIntents().YES_AGENT_TRANSFER) {
                response = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                response = await this.nlpHandler.fetchFinalResponse(response, param);
                response.parameters.agent = true;
            } 
            else if (flowName == ConstantUtil.getFlows().AUTH_FLOW) {
               console.log("-------------auth------------------------------------")
                console.log(param)
                let dob = param.PARAMETERS.dob[0].split('-').reverse()
                let temp = dob[0]
                 dob[0] = dob[1]
                 dob[1] = temp

             //October 5th 1999
             if(dob.join('-')===param.PARAMETERS.date_of_birth){
                 console.log("+++++++++++++++==UTH============================")
                 param.PARAMETERS.confirmed_entities++
                   let msg = MessageUtil.getMessage().NAME_SUCCESS
                 let flowName=''
                if( param.PARAMETERS.confirmed_entities<3) {
                    flowName = ConstantUtil.getFlows().MEMBER_PHONE
                }
                 response = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().NAME_SUCCESS);
                 response = await this.nlpHandler.fetchFinalResponse(response, param,flowName);


                }else {
                 console.log("------------------------------error dob-------------------------------------------------")
                 param.PARAMETERS.dob_error_count = param.PARAMETERS.dob_error_count ? param.PARAMETERS.dob_error_count : 0
                 param.PARAMETERS.dob_error_count++;

                 if (param.PARAMETERS.dob_error_count >= config.get("agent:error_count")) {

                     let flowName = ConstantUtil.getFlows().MEMBER_PHONE
                     let msg = MessageUtil.getMessage().MEMBER_DOB_NOT_FOUND
                     param.PARAMETERS.error_count++;
                     if(param.PARAMETERS.error_count === 2){
                         console.log("faiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiilureeeeeeeeeeee")
                         flowName = ConstantUtil.getFlows().AGENT_TRANSFER
                         msg = MessageUtil.getMessage().AGENT_TRANSFER
                         param.followup_event="";
                         param.PARAMETERS.agent=true;
                     }
                     response = await this.nlpHandler.fetchTextJson(msg);
                     response = await this.nlpHandler.fetchFinalResponse(response, param, flowName);

                     //Added by Ayush
                     // param.PARAMETERS.subscriber_id_count = 0;
                     // json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Member id "+val+". Let's try with NPI number. ");
                     // json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                     // response.parameters.agent = true;
                 }
                 else
                     {


                         response = await this.nlpHandler.fetchTextJson(`${MessageUtil.getMessage().MEMBER_DOB_NOT_FOUND}`);
                         response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().AUTH_FLOW);


                         response.reset_entities = ['date_of_birth']
             }



             }
                 //response = await this.hippaVerification(param);
                // response = await this.getDataOnMemberID(param)
            }
         else if (flowName == ConstantUtil.getFlows().MEMBER_PHONE) {
             console.log("-------------phoneeeeeeeeee------------------------------------")
             console.log(param)
             let phone = param.PARAMETERS.phone[0].split('-').join("")
             console.log(phone)
             console.log(param.PARAMETERS.mobile_number)
             //October 5th 1999
             if(phone===param.PARAMETERS.mobile_number){
                 console.log("+++++++++++++++==UTH============================")
                 response = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().PHONE_SUCCESS);
                 response = await this.nlpHandler.fetchFinalResponse(response, param);


             }else{

                 param.PARAMETERS.phone_error_count = param.PARAMETERS.phone_error_count ? param.PARAMETERS.phone_error_count : 0
                 param.PARAMETERS.phone_error_count++;

                 if (param.PARAMETERS.phone_error_count >= config.get("agent:error_count")) {




                     console.log("---------------error again---")
                     param.followup_event="";
                     param.PARAMETERS.agent=true;
                     response = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                     response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().AGENT_TRANSFER)
                     // response = await this.nlpHandler.fetchTextJson(`${MessageUtil.getMessage().MEMBER_PHONE_NOT_FOUND}`);
                     // response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().MEMBER_PHONE);



                 }else{

                     response = await this.nlpHandler.fetchTextJson(`${MessageUtil.getMessage().MEMBER_PHONE_NOT_FOUND}`);
                     response = await this.nlpHandler.fetchFinalResponse(response, param, ConstantUtil.getFlows().MEMBER_PHONE);
                     response.reset_entities = ['mobile_number'];

                 }



             }
             //response = await this.hippaVerification(param);
             // response = await this.getDataOnMemberID(param)
         }
             else if (intentName == ConstantUtil.getIntents().CASE_REFERENCE) {
                response = await this.caseRef(param);
            } 

            else if (flowName==ConstantUtil.getFlows().MEMBER_DETAILS){
                response = await this.npiDetails(param)

            }

            else if (flowName==ConstantUtil.getFlows().MEMBER_ADDRESS){
                response = await this.memberAddressDetails(param)
            }
            // else if (flowName == ConstantUtil.getFlows().MEMBER_INFO) {
            //     response = await this.multipleAuthorizations(param);
                
            // }

            // else if (intentName ==  ConstantUtil.getIntents().YES_INETNT) {
            //     console.log("inside flowname mem id yes")
            //     // response = await this.multipleAuthorizations(param);
            //     response = await this.getDataOnMemberID(param)
            // }
            console.log(" response --------------- ", response)
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            res.end();
        } catch (e) {
            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    };

    async execute2(req,res){
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try{
            const parameters = req.body.parameters
            const param = this.nlpHandler.fetchNlpParams(req.body);
            const intentName = req.body.intent_name
            const flowName = req.body.flow_name;
            param.flowName = flowName;
            param.intentName = intentName;
            param.npiID = req.body.parameters.npiID;
            param.npiValue = this.validateParam(req.body.parameters.npi_id);
            param.session_id=req.body.session_id;
            param.accessToken = req.body.access_token;
            param.startDate = req.body.parameters.start_date;
            param.start_date = req.body.parameters.startD;
            param.endDate = req.body.parameters.end_date;
            param.memberId = this.validateParam(req.body.parameters.subscriber_id);
            param.memberDOB = req.body.parameters.date_of_birth;
            param.refNumber = this.validateParam(req.body.parameters.c_ref);
            param.authID = req.body.parameters.auth_id;
            param.house_number = req.body.parameters.house_number;
            param.houseNumber=req.body.parameters.houseNumber;
            param.street_name = req.body.parameters.street_name;
            param.streetName=req.body.parameters.streetName;
            param.taxId = this.validateParam(req.body.parameters.tax_id);
            param.ZIP=req.body.parameters.ZIP;
            param.check_status = req.body.parameters.check_status_type;
            delete param.PARAMETERS.agent;

            let response = {}
            let data ={}
            switch(flowName){
                case ConstantUtil.getFlows().MEMBER_ID_FLOW :
                    console.log("--------------------member id--------------")
                    data = await this.confirm(parameters,flowName,intentName,param)
                    break
                case ConstantUtil.getFlows().MEMBER_ID_YES :
                    console.log("--------------------yess--------------")
                    data = await this.getDataOnMemberID(parameters,flowName,intentName,param)
                    break
                case ConstantUtil.getFlows().MEMBER_NAME:
                    console.log("--------------------first name--------------")
                    data = await this.verifyFName(parameters,flowName,intentName,param)
                    break
                case ConstantUtil.getFlows().MEMBER_LNAME :
                    console.log("--------------------last name--------------")
                    data = await this.verifyLName(parameters,flowName,intentName,param)
                    break
                case ConstantUtil.getFlows().AUTH_FLOW :
                    console.log("--------------------DOB--------------")
                    data = await this.verifyDateOfBirth(parameters,flowName,intentName,param)
                    break
                case ConstantUtil.getFlows().MEMBER_PHONE :
                    console.log("--------------------PHONE--------------")
                    data = await this.verifyPhoneNumber(parameters,flowName,intentName,param)
                    break
                // case ConstantUtil.getFlows().CASE_REFERENCE :
                //     break
                // case ConstantUtil.getFlows().MEMBER_DETAILS :
                //     break
                // case ConstantUtil.getFlows().MEMBER_ADDRESS :
                //     break
                // case (ConstantUtil.getFlows().AGENT_TRANSFER || intentName === ConstantUtil.getIntents().YES_AGENT_TRANSFER) :
                //     break

            }

            response = await this.nlpHandler.fetchTextJson(data.msg);
            response = await this.nlpHandler.fetchFinalResponse(response, param, data.nextFlow);
            if(data.agent && data.agent===true){
                response.parameters.agent = true;
                response.followup_event="";
            }
            if(data.resetEntities && data.resetEntities.length>0){
                response.reset_entities=data.resetEntities
            }
            console.log(" response --------------- ", response)
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            res.end();

        }catch(err){
            console.log('Error!', err);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    }

    /*
    async postReq(key,type,data) {


        let req = this.config.get("rgil:"+key);
        let options = {
            headers: {
                "Content-Type": "text/xml",
                }
        }

        let path;


        if(type == "policynumber"){

            // const path = this.config.get(req+":url"+`${param.parameters.policynumber}`);

            path = req.url+data;
            console.log("-----------path is ",path);
            return this.restUtil.getRequest(path,options);

        }else if(type == "policyOnDigit"){

           let path1 = "http://rgidwh01.reliancegeneral.co.in/rgiwebservice/Service1.asmx?wsdl";
           let d = await this.restUtil.postRequest(path1,data,options);
           let r = XmljsonUtil.xmltojson(d,type);
           console.log("----------------result-----------",r);
           return r;

        }
     */

    async callDefault(request, res) {
        res.set("Access-Control-Allow-Origin", "*");
        res.set("Access-Control-Allow-Headers", "X-Requested-With");
        try {
            let response = {}, reqbody = request.body;
            const param = this.nlpHandler.fetchNlpParams(reqbody);
            console.log(" callDefault ++++++++++++", reqbody);
            response = await this.nlpHandler.fetchTextJson("Please try again");
            response = await this.nlpHandler.fetchFinalResponse(response, param);
            res.setHeader('Content-Type', 'application/json');
            res.send(response);
            res.end();
        } catch (e) {
            console.log('Error!', e);
            res.setHeader('Content-Type', 'application/json');
            res.send(this.nlpHandler.fetchUnableProcessResponse());
            res.end();
        }

    };

    async hippaVerification(param) {
        let json = {};
        let result = await this.userService.hippaVerification(param);
        //console.log("hippaVerification ==========  ", result)
        if (result && result.subscriberId
            // &&
            // result.subscriberId == param.memberId &&
            // result.serviceDate == param.startDate &&
            // result.serviceDateTo == param.endDate
        ) {
            param.PARAMETERS.error_count = 0;
            param.PARAMETERS.fName = result.firstName;
            param.PARAMETERS.lName = result.lastName;
            json = await this.nlpHandler.fetchTextJson("Okay");
            json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_NAME);
            return json;
        } else {
            if (param.PARAMETERS.error_count >= config.get("agent:error_count")) {
                json = this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                json = this.nlpHandler.fetchFinalResponse(json, param);
                json.parameters.agent = true;
            } else {
                let reset_entities=null,redirectFlowName="", error = result.response.data.exceptions && result.response.data.exceptions.length > 0?result.response.data.exceptions[0]:{};
                console.log("status ==========  ",JSON.stringify(result.response.data))
                if(error.code){
                    error.message=MessageUtil.getMessage()[error.code];
                    param.PARAMETERS[error.code+'_count']=param.PARAMETERS[error.code+'_count']?param.PARAMETERS[error.code+'_count']:0
                    param.PARAMETERS[error.code+'_count']=param.PARAMETERS[error.code+'_count']+1;
                }
                
                switch(error.code){
                   case 'ENT_ME_72':
                        redirectFlowName = ConstantUtil.getFlows().MEMBER_ID_FLOW;
                        break;
                   case 'ENT_ME_router_2100':
                        reset_entities = ['subscriber_id'];
                        redirectFlowName = ConstantUtil.getFlows().MEMBER_ID_FLOW;
                        break;
                    case 'ENT_ME_71':
                        reset_entities = ['date_of_birth'];
                        redirectFlowName = ConstantUtil.getFlows().AUTH_FLOW;
                        break;
                    case 'ENT_ME_1003':
                        reset_entities = ['date_of_birth'];
                        redirectFlowName = ConstantUtil.getFlows().AUTH_FLOW;
                        break;
                    case 'ENT_ME_62':
                        reset_entities = ['start_date'];
                        redirectFlowName = ConstantUtil.getFlows().AUTH_FLOW;
                        break;
                    default:
                        param.PARAMETERS.error_count=param.PARAMETERS.error_count?param.PARAMETERS.error_count:0
                        param.PARAMETERS.error_count++;
                        error.message = MessageUtil.getMessage().MEMBER_NOT_FOUND + this.correctForPolly(param.memberId)
                        reset_entities = ['subscriber_id','date_of_birth','start_date'];
                        redirectFlowName = ConstantUtil.getFlows().MEMBER_ID_FLOW;
                        break;
                }
                if (param.PARAMETERS.error_count >= config.get("agent:error_count") || (error.code && (param.PARAMETERS[error.code+'_count'] >= config.get("agent:error_count")))) {
                    json = this.nlpHandler.fetchTextJson(error.message + " " + MessageUtil.getMessage().AGENT_TRANSFER);
                    json = this.nlpHandler.fetchFinalResponse(json, param);
                    json.parameters.agent = true;
                }else{
                    json = this.nlpHandler.fetchTextJson(error.message);
                    json = this.nlpHandler.fetchFinalResponse(json, param,redirectFlowName);
                    json.reset_entities = reset_entities;
                }
                
            }
            return json;
        }

    };

    // async multipleAuthorizations(param) {
    //     let result = await this.userService.multipleAuthorizations(param);
    //     let json = {}
    //     if (result && result.utilizationManagementCaseSummaryListType) {
    //         let caseSummary = this.getCodeData(result, param.check_status);
    //         //console.log(caseSummary);
    //         if (caseSummary.multiple) {
    //             caseSummary = caseSummary.caseIdentifier + MessageUtil.getMessage().MORETHEN_ONE_STATUS
    //             json = await this.nlpHandler.fetchTextJson(caseSummary);
    //             json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().TALKTOAGENT_FLOW);
    //             json.parameters.agent = false;
    //         } else {
    //             caseSummary = caseSummary.code;
    //             json = await this.nlpHandler.fetchTextJson(caseSummary);
    //             json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().TALKTOAGENT_FLOW);
    //         }
    //         return json;
    //     } else {
    //         param.PARAMETERS.error_count=param.PARAMETERS.error_count?param.PARAMETERS.error_count:0
    //         param.PARAMETERS.error_count++;
    //         let reset_entities=null;
    //         let error = result.response.data.exceptions && result.response.data.exceptions.length > 0?result.response.data.exceptions[0]:{};
    //         console.log(JSON.stringify(result.response.data));
    //         if(error.code){
    //             param.PARAMETERS[error.code+'_count']=param.PARAMETERS[error.code+'_count']?param.PARAMETERS[error.code+'_count']:0
    //             param.PARAMETERS[error.code+'_count']=param.PARAMETERS[error.code+'_count']+1;
    //         }
    //         switch(error.code){
    //             case 'ENT_UI_5007':
    //                  reset_entities = ['end_date'];
    //                  break;
    //             case 'ENT_UI_5005':
    //                 reset_entities = ['start_date'];
    //                 break;
    //             case 'ENT_UI_5006':
    //                     reset_entities = ['end_date'];
    //                     break;
    //             default:
    //                  let val = this.correctForPolly(param.npiValue);
    //                  error.message = MessageUtil.getMessage().NPI_NOT_FOUND+val;
    //                  reset_entities = ['npi_id'];
    //                  break;
    //          }
    //         if (param.PARAMETERS.error_count >= config.get("agent:error_count")  || (error.code && (param.PARAMETERS[error.code+'_count'] >= config.get("agent:error_count")))) {
    //             json = await this.nlpHandler.fetchTextJson(error.message + " " + MessageUtil.getMessage().AGENT_TRANSFER);
    //             json = await this.nlpHandler.fetchFinalResponse(json, param);
    //             json.parameters.agent = true;
    //         } else {
    //             json = await this.nlpHandler.fetchTextJson(error.message);
    //             json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
    //             json.reset_entities = reset_entities;
    //         }
    //         return json;
    //     }
    // };


     async memberAddressDetails(param){
 

        let json = {};
        let isMatched=(param.house_number == param.houseNumber)
        if (isMatched) {
            param.PARAMETERS.house_number_count = 0;
            if(param.taxId  ==  param.ZIP){
                param.PARAMETERS.tax_id_count = 0;
                json = await this.nlpHandler.fetchTextJson("I have located and verified the member. Let’s proceed.");
                json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().AUTHORIZATION_ID_FLOW);
                return json;
                // if(param.street_name == param.streetName){
                //     param.PARAMETERS.street_name_count = 0;

                //     //console.log(caseSummary);
                //     // json = await this.nlpHandler.fetchTextJson("I have located and verified the member. Let’s proceed.");
                //     // json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().AUTHORIZATION_ID_FLOW);
                //     // return json;
                // }else{
           
                //     param.PARAMETERS.street_name_count=param.PARAMETERS.street_name_count?param.PARAMETERS.street_name_count:0
                //     param.PARAMETERS.street_name_count++;
                //     if (param.PARAMETERS.street_name_count >= config.get("agent:error_count")) {
                //         // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                //         // json = await this.nlpHandler.fetchFinalResponse(json, param);
                //         //Added by Ayush
                //         param.PARAMETERS.street_name_count = 0;
                //         json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details on the provided stree name let me connect you to the agent");
                //         json = await this.nlpHandler.fetchFinalResponse(json, param);
                //         json.parameters.agent = false;
                //     } else {
                //         json = await this.nlpHandler.fetchTextJson('please provide the accurate street name');
                //         json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().CORRECT_MEMBER_DETAILS);
                //         json.reset_entities = ['street_name'];
                //     }
                   
                
            //   }
    
       }
            else{

                param.PARAMETERS.tax_id_count=param.PARAMETERS.tax_id_count?param.PARAMETERS.tax_id_count:0
                param.PARAMETERS.tax_id_count++;
                if (param.PARAMETERS.tax_id_count >= config.get("agent:error_count")) {
                    // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                    // json = await this.nlpHandler.fetchFinalResponse(json, param);
                    //Added by Ayush
                    param.PARAMETERS.tax_id_count = 0;
                    json = await this.nlpHandler.fetchTextJson("I am mot able to fetch details on the provided zip code ");
                    json = await this.nlpHandler.fetchFinalResponse(json, param);
                    json.parameters.agent = false;
                } else {
                    json = await this.nlpHandler.fetchTextJson('please provide accurate zip code');
                    json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().MEMBER_ADDRESS);
                    json.reset_entities = ['tax_id'];
                }
                return json;
                


            }
            
        } else {
            param.PARAMETERS.house_number_count=param.PARAMETERS.house_number_count?param.PARAMETERS.house_number_count:0
            param.PARAMETERS.house_number_count++;
            // let val = this.correctForPolly(param.npiValue);
            if (param.PARAMETERS.house_number_count >= config.get("agent:error_count")) {
                // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                // json = await this.nlpHandler.fetchFinalResponse(json, param);
                //Added by Ayush
                param.PARAMETERS.house_number_count = 0;
                json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Case Reference number "+val+". Let's try with NPI number. ");
                json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                json.parameters.agent = false;
            } else {
                json = await this.nlpHandler.fetchTextJson('please provide correct house number');
                json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().MEMBER_ADDRESS);
                json.reset_entities = ['house_number'];
            }
            return json;
        }
    }

        // console.log("----------data1 is ---------",falsekey)
        // var found = data.find(function(d) { 
        //     if(d.value==false){
        //         falsekey.push(d.key)
        //         return falsekey
        //     } 
        //   }); 



    async caseRef(param) {
        
        
        let json = {};


        console.log("refNumber:",param.refNumber);
        // console.log("c_refNumber:",parameters.c_ref);
        console.log("auth_id:",param.authID)
        let isMatched=(param.refNumber == param.authID)
               
        if (isMatched) {
            param.PARAMETERS.c_ref_count = 0;
            let caseSummary = this.getCodeData(param.PARAMETERS.Status,param.authID);
            //console.log(caseSummary);
            json = await this.nlpHandler.fetchTextJson(caseSummary);
            json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().TALKTOAGENT_FLOW);
            return json;
        } else {
            param.PARAMETERS.c_ref_count=param.PARAMETERS.c_ref_count?param.PARAMETERS.c_ref_count:0
            param.PARAMETERS.c_ref_count++;
            let val = this.correctForPolly(param.refNumber);
            if (param.PARAMETERS.c_ref_count >= config.get("agent:error_count")) {
                // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                // json = await this.nlpHandler.fetchFinalResponse(json, param);
                //Added by Ayush
                param.PARAMETERS.c_ref_count = 0;
                json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Case Reference number "+val+". Let's try with NPI number. ");
                json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                json.parameters.agent = false;
            } else {
                json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().NOT_VERIFIED_MAIN_MENU+val);
                json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().AUTHORIZATION_ID_FLOW);
                json.reset_entities = ['c_ref'];
            }
            return json;
        }

    };


    async npiDetails(param){
        let json = {};
        console.log("npi_id:",param.npiValue );
        // console.log("c_refNumber:",parameters.c_ref);
        console.log("npiID",param.npiID);
        // console.log("parameters npiID",parameters.npiID);

        let isMatched=(param.npiValue == param.npiID)
        if (isMatched) {
            param.PARAMETERS.npi_id_count = 0;
            console.log("startDate:",param.startDate);
            console.log("start_date:",param.start_date);
            if(param.startDate == param.start_date){
                param.PARAMETERS.start_date_count = 0;
                let caseSummary = this.getCodeData(param.PARAMETERS.Status,param.authID);
                //console.log(caseSummary);
                json = await this.nlpHandler.fetchTextJson(caseSummary);
                json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().TALKTOAGENT_FLOW);
                return json;
            }else{

                param.PARAMETERS.start_date_count=param.PARAMETERS.start_date_count?param.PARAMETERS.start_date_count:0
                param.PARAMETERS.start_date_count++;
                if (param.PARAMETERS.start_date_count >= config.get("agent:error_count")) {
                    // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                    // json = await this.nlpHandler.fetchFinalResponse(json, param);
                    //Added by Ayush
                    param.PARAMETERS.start_date_count = 0;
                    json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details on the provided start date "+val);
                    json = await this.nlpHandler.fetchFinalResponse(json, param);
                    json.parameters.agent = false;
                } else {
                    json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().SERVICE_START_DATE);
                    json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().MEMBER_DETAILS);
                    json.reset_entities = ['start_date','npi_id'];
                }
                return json;
                


            }
            
        } else {
            param.PARAMETERS.npi_id_count=param.PARAMETERS.npi_id_count?param.PARAMETERS.npi_id_count:0
            param.PARAMETERS.npi_id_count++;
            let val = this.correctForPolly(param.npiValue);
            if (param.PARAMETERS.npi_id_count >= config.get("agent:error_count")) {
                // json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().AGENT_TRANSFER);
                // json = await this.nlpHandler.fetchFinalResponse(json, param);
                //Added by Ayush
                param.PARAMETERS.npi_id_count = 0;
                json = await this.nlpHandler.fetchTextJson("Since I am not able to fetch details through provided Case Reference number "+val+". Let's try with NPI number. ");
                json = await this.nlpHandler.fetchFinalResponse(json, param,ConstantUtil.getFlows().MEMBER_INFO);
                json.parameters.agent = false;
            } else {
                json = await this.nlpHandler.fetchTextJson(MessageUtil.getMessage().NOT_VERIFIED_MAIN_MENU+val);
                json = await this.nlpHandler.fetchFinalResponse(json, param, ConstantUtil.getFlows().AUTHORIZATION_ID_FLOW);
                json.reset_entities = ['npi_id','start_date'];
            }
            return json;
        }
    }

    getCodeData(data,authID) {
        let msg = ""
        if(data=="Pending"){
           msg = `Your authorization request ${authID} is currently being reviewed by our clinical team. Rest assured we will send you the determination once done on the fax number 999-999-9997 you have given. Thank you!
If you need additional information, please press 1 now or state: I need to speak with a representative. If you do not need additional information or services, you may disconnect the call by hanging up.`
      }else if(data=="Denied"){
            msg = `Your Authorization Request number ${authID} for CPT 62323 has been denied due to medical necessity not met.
            A fax with this information was sent to 999-999-9997. The fax included information regarding appeal and or grievance rights and the process for requesting an appeal or grievance.
            A mail was also sent on House number 1004, Lincoln Road, California with this information.If you need the notice of determination re-faxed, you may state “re-fax” or select number 2 on your keypad now.
            If any part of the request needs to be changed, you will need to either utilize the portal to make the changes, call back and request to speak with a representative, or you may please press 1 now or state: I need to speak with a representative. 
            If you do not need additional information or services, you may disconnect the call by hanging up.
            `
        }else if (data=="Active"){
            msg = `Your authorization request ${authID} for CPT 62323, for Dr. John Smith, has been approved. Authorization is valid from July 1st 2020 to July 31st 2020. Please note that approvals are not guarantee for coverage or payment. 
            A fax with this information was sent to number 999-999-9997 on May 15th 2020. If you need the notice of determination re-faxed, you may state “re-fax” or select number 2 on your keypad now.
            If any part of the approved request needs to be changed, you will need to either utilize the portal to make the changes, call back and request to speak with a representative, or you may please press 1 now or state: I need to speak with a representative. 
            If you do not need additional information or services, you may disconnect the call by hanging up.
            `
        }

        return msg;
        
    }

    validateParam(str) {
        if(str){
            str = str.replace(/-/g, "").replace(/ /g, "").replace(/\+/g, "");
        }else{
            str=null;
        } 
        return str;
    }

    correctForPolly(str){
      if(str)
      str = str.split("").join(" ");
      return str;
    }

    dateFormat(dt){
        var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
        ];
        var d=new Date(dt);
        var curr_date = d.getDate();
        var curr_month = monthNames[d.getMonth()]; //Months are zero based
        var curr_year = d.getFullYear();
        var transcription=curr_date+" "+curr_month+" "+curr_year;
        return transcription;
    }


}

module.exports = WebhookController;
