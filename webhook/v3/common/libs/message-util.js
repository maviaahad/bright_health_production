class MessageUtil{

    static getMessage(){
         return {
            ENT_ME_72:"I could not find member details on provided ID. Lets try again.",
            ENT_ME_router_2100:"I could not find member details on provided ID. Lets try again.",
            ENT_ME_71:"Member date of birth provided by you does not match our records.",
            ENT_ME_1003:"Member date of birth provided by you does not match in the format MM DD YYYY.",
            ENT_ME_62:"Provided service start date is out of range.",
            FIRST_NAME_FAIL:"I could not match details provided by you as per first name. Lets try again.",
            LAST_NAME_FAIL:"I could not match details provided by you as per last name. Lets try again.",
            NAME_SUCCESS:"Okay",
            PHONE_SUCCESS:"Thank you. Member has been successfully verified.",
            ASK_MEMBER_NAME:"please provide member's name",
            NPI_ID_FAIL:"Since i am not able to verify NPI number ",
            AGENT_TRANSFER:"Let me connect you to the next available agent, who can assist you further.",
            VERIFY_THANKS:"Thank you for providing your details",
            TAX_ID:"Thank you verifing your tax id",
            MEMBER_NAME:"Thanks for providing your name",
            MEMBER_ID:"I will need one more info to verify ",
            MEMBER_NOT_FOUND:"I am not able to find any details on member id ",
            MEMBER_DOB_NOT_FOUND:"I am not able to find any details on member dob ",
             MEMBER_PHONE_NOT_FOUND:"I am not able to find any details on member phone number ",
            NPI_NOT_FOUND:"I am not able to find any details on NPI number ",
            NOT_VERIFIED_USER:"I am not able to find any user on the provided details",
            NOT_VERIFIED_MAIN_MENU:"The information provided is not found in the system with case reference number ",
            MORETHEN_ONE_STATUS:" has more than one status available. ",
            SERVER_DOWN:"This appears to be an exception.Let me connect to connect you to an agent for further assistance",
            SERVICE_START_DATE:"Please provide the accurate service start date"
        }
    }

    static get(){
        if(arguments.length < 1)
        return '';
       let key = this.getMessage()[arguments["0"]];
       for(let i in arguments){
        if(i != "0")
         key = key.replace("@ARGU@",arguments[i]);
       }
       return key;
    }

}

module.exports = MessageUtil;