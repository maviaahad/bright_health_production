const fs = require('fs')
const parseString = require('xml2js').parseString;

class XmljsonUtil{

    static xmltojson(data,type){

        console.log("-------------------------type",type);
        let result1='';
        // console.log(data)
        if(type=="memberId"){
            parseString(data, function(err, result){
                if(err) console.log(err);
                result1 = result['S:Envelope']['S:Body'][0]['ns34:findMemberSummaryResponse'][0]['return']
                //  result1 = JSON.stringify(result2);
            })}
        else if (type=="policynumber"){
            console.log("-------------------- type of data  ",typeof data)

            let text = data.replace(/&lt;/g, "<").replace(/&gt;/g, ">")
            parseString(text, function(err, result){
                if(err) console.log(err);
                result1 =  result.string.PolicyDetails[0]
            })

        }
        else if(type == "policyOnDigit"){
            parseString(data, function(err, result){
                if(err) console.log(err);


                let obj = result['soap:Envelope']['soap:Body'][0]['GetMotordetailsforwebsiteResponse'][0]['GetMotordetailsforwebsiteResult'][0]['diffgr:diffgram'][0]['NewDataSet'][0]['Table']

                let data = obj.map(o => o.policyno[0] )
                let last_digit = '8388'+"$"
                let regexConst = new RegExp(last_digit);

                console.log("----------------regex typeof", regexConst);



                let responseData = data.find(function (d) {

                    let result2 = regexConst.test(d);
                    console.log("---------------------result 2",result2)
                    return result2
                });

                var filteredData = obj.filter(function(obj) {
                    return  obj.policyno[0] == responseData;
                });

                console.log("-------------------- data found",filteredData)

                result1= filteredData

            })}
        return result1


    }




}

module.exports = XmljsonUtil
