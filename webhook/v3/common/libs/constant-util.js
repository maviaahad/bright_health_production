class ConstantUtil{

    static getFlows(){
        return {
             WELCOME:"welcome",
             AUTHORIZATION_STATUS:"authorization_status",
             AUTH_FLOW:"auth_flow",
             //MEMBER_ID_FLOW:"member_id_flow",
             MEMBER_LNAME:"member_lname",
             MEMBER_NAME:"member_name",
             MAIN_MENU_FLOW:"main_menu_flow",
             TALKTOAGENT_FLOW:"talktoagent_flow",
             //MEMBER_INFO:"m_detail",
             MEMBER_DETAILS:"m_detail",
             AUTHORIZATION_ID_FLOW:"authorization_id_flow",
             MEMBER_ADDRESS:"member_address",
             MEMBER_ID_FLOW:"member_id_flow",//-----------------------
             MEMBER_ID_YES:"mem_id_yes",
             CASE_REFERENCE_FLOW:"case_ref_flow",
             AGENT_TRANSFER:"agent_transfer_flow",
             NPI_ID_YES:"npi_id_yes",
             CASE_REF_YES:"case_ref_yes",
             START_DATE_YES:"start_date_yes",
             YES_AGENT_TRANSFER:"yes_transfer_to_agent",
             NO_MAIN_MENU:"no_main_menu",
             TAX_ID_FLOW:"tax_id_flow",
             SAME_MEMBER:"same_member",
             MEMBER_PHONE:"member_phone",
             CORRECT_MEMBER_DETAILS:"Correct_member_details",
            THANKYOU_MEMBER_DETAILS:"Thank_you_MemberDetails"
            }
    } 

    static getIntents(){
        return {
            YES_AGENT_TRANSFER:"yes_transfer_to_agent_intent",
            YES_INETNT:"yes_intent",
            NO_MAIN_MENU:"no_main_menu_intent",
            NPI_ID_INTENT:"npi_id_intent",
            CASE_REFERENCE:"case_reference",
            MEMBER_DETAILS:"member_details_flow",
            SERVICE_DATE:"service_date_flow",
            TAX_ID_INTENT :"tax_id_intent",
            MEMBER_NAME_INTENT:"member_name_intent",
            MEMBERID_INTENT:"memberID_intent",//--------------------
            DATE_OF_BIRTH_INTENT:"date_of_birth_intent",
            SERVICE_DATE_INTENT:"service_date_intent",

        }
    }

    static  getFlowMapping(){
        return {
            tax_id_flow:{
                field:"tax id",
                key:"tax_id"
            },
            member_id_flow:{
                field:"member id",
                key:"subscriber_id"
            }
        }
    } 

    static  getResetMapping(){
        return {
            taxid_no:{
                redirect:"tax_id_flow",
                message:"Since you are not able to confirm your tax id.",
                key:"tax_id"
            },
            mem_id_no:{
                redirect:"member_id_flow",
                message:"Since you are not able to confirm your member id.",
                key:"subscriber_id"
            }
        }
    } 

}

module.exports = ConstantUtil;