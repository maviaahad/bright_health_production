const nlpConstants = require('./nlp-constants');
const RASAHandler = require('./humonics/rasa-handler');

class NLPHandler {
    constructor(config) {
        this.platform = config.get('nlp:platform');
        this.version = config.get('nlp:version');
        this.rasaHandler = new RASAHandler(config);
    }

    fetchNlpParams(request) {
        console.log('INSIDE NLP PARAMS');
        try {
            let params = {};
            if (this.platform === 'Humonics') {
                params = this.rasaHandler.fetchNlpParams(request);
            }
            return params;
        } catch (err) {
            throw new Error(err);
        }
    };

    async fetchUnableProcessResponse() {
        let json = this.fetchTextJson(nlpConstants.UNABLE_TO_PROCESS_REQUEST);
        json = this.fetchFinalResponse(json,{});
        return json;
    };

    async fetchSlotJson(text, disable, buttons) {
        console.log("BUILDING SLOT_TYPE RESPONSE");
        try {
            let response;
            if (this.platform === 'Humonics') {
                response=  this.fetchButtonJson(text, disable, buttons);
            }
            return response;
        } catch (err) {
            throw new Error(err)
        }
    };

    // fetchFollowupEventJson(name, parameters) {
    //     let json = {};

    //     return json;
    // };

    fetchTextJson(response) {
        let json = {};
        if (this.platform === 'Humonics') {
            json = this.rasaHandler.fetchTextJson(response);
        }
        return json;
    };

    fetchImageJson(text, images) {
        let json = {};
        if (this.platform === 'Humonics') {
            json = this.rasaHandler.fetchImageJson(text, images);
        }
        return json;
    };

    fetchHtmlJson(html) {
        let json = {};
        if (this.platform === 'Humonics') {
            json = this.rasaHandler.fetchHtmlJson(html);
        }
        return json;
    };

    fetchButtonJson(text, disable, buttons) {
        let json = {};
        if (this.platform === 'Humonics') {
            json = this.rasaHandler.fetchButtonJson(text, disable, buttons);
        }
        return json;
    };

    fetchChartTemporaryJson(data) {
        let json = {};
        if (this.platform === 'Humonics') {
            //TODO: In future
        }
        return json;
    };

    fetchTableJson(data) {
        let json = {};
        if (this.platform === 'Humonics') {
            //TODO: In future
        }
        return json;
    };

    fetchFinalResponse(response, params,followup_event) {
        if (this.platform === 'Humonics') {
            response = this.rasaHandler.finalResponse(response, params,followup_event);
        }
        return response;
    }

}

module.exports = NLPHandler;