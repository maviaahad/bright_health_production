//const nlpConstants = require('../nlp-constants');

class RASAHandler {
    constructor(config) {
        this.version = config.get('nlp:version');
    }

    fetchNlpParams(request) {
        let params = {};
        if (this.version === 'v1') {
            params = {
                USER_QUERY: request.user_query,
                PARAMETERS: request.parameters?request.parameters:{},
                USER_IDENTITY: request.parameters?request.parameters['userIdentity']:null,
                PHONE: request.parameters?request.parameters['userIdentity']:null,
                PHONE_NUMBER: request.parameters?request.parameters['phone_number']:null
            }
        }
        return params;
    };

    fetchTextJson(response) {
        let json ={};
        if (this.version === 'v1') {
            json = {
                    "template_type": 'text',
                    "text": response
                }
        }
        return json;
    };

    finalResponse(response,params,followup_event){
        if (this.version === 'v1') {
            response = {
                "response":response,
                "parameters": params.PARAMETERS,
                "followup_event":followup_event?followup_event:""
            };
        }
        return response;
    }

    fetchButtonJson(text, disable, buttons) {
        let json ={};
        if (this.version === 'v1') {
            json = {
                "template_type": 'button',
                "text": text,
                "buttons":buttons
            }
        }
        return json;
    };

    fetchImageJson(text, images) {
        let json ={};
        if (this.version === 'v1') {
            json = {
                "template_type": 'image',
                "text": text,
                "images":images
            }
        }
        return json;
    };

    fetchHtmlJson(html) {
        let json ={};
        if (this.version === 'v1') {
            json = {
                "template_type": 'html',
                "data": html
            }
        }
        return json;
    };

}

module.exports = RASAHandler;